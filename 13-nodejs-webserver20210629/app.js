const fs = require('fs');
const http = require('http');
const port = 3000;

const renderHTML = (path, res) => {
    fs.readFile(path, (e,d) => {
        console.log(e);
        if(e){
            res.writeHead(404);
            res.write('Halaman Ga ada Cuk');
        }else{
            res.write(d);
        }
        res.end();
    });
}

const server = http.createServer((req, res) => {    
    res.writeHead(200, {
        'Content-Type' : 'text/html',
    })

    const url = req.url;

    switch(url) {
        case '/about':
            renderHTML('./about.html',res);
            break;
        case '/contact':
            renderHTML('./contact.html',res);
            break;
        default:
            renderHTML('./index.html', res);
            break;
    }



    // if(url === '/about'){
    //     renderHTML('./about.html', res)
    // }else if(url === '/contact'){
    //     renderHTML('./contact.html', res);
    // }else {
    //     renderHTML('./index.html',res);
    // }

    
});

server.listen(port, () => {
    console.log(`server is listening on port ${port}.`);
});