const fs = require('fs');
// const readline = require('readline');

// const rl = readline.createInterface({
//   	input: process.stdin,
//   	output: process.stdout
// });

// Membuat Folder Data.
const dirPath = './data';
if(!fs.existsSync(dirPath)){
	fs.mkdirSync(dirPath);
}

// Membuat File contacts.json jika belom ada
const dataPath = './data/contacts.json';
if(!fs.existsSync(dataPath)){
	fs.writeFileSync(dataPath, '[]', 'utf-8');
}

// const tanya = (pertanyaan) => {
//     return new Promise((resolve, rejects) => {
//         rl.question(pertanyaan, (nama) => {
//             resolve(nama);
//         });
//     });
// };

const loadContact = () => {
    const fileJson 	= fs.readFileSync('data/contacts.json', 'utf8');
    const fileSave  = JSON.parse(fileJson);
    return fileSave;
}

const simpanContact = (nama, tinggal, perkawinan, anak, tgl_nikah) => {
    const jawaban 	= { nama , tinggal, perkawinan, anak, tgl_nikah }
    const fileSave  = loadContact();

    // Cek Duplikat
    const duplikat2 = fileSave.find((jawaban) => jawaban.nama === nama);
    if(duplikat2){
        console.log('COntact Sudah Terdaftar');
        return false;
    }

    fileSave.push(jawaban);

    fs.writeFileSync('data/contacts.json', JSON.stringify(fileSave, null, 2), (err) => {
        console.log(err);
    });

    // console.log(fileSave);
    console.log(`Oke, Terimakasih ${nama}`);
    // rl.close();
}

const listContact = () => {
    const fileData      = loadContact();
    console.log('Daftar Kontak');
    fileData.forEach((fileData, i) => {
        console.log(`${i +1}. ${fileData.nama} ${fileData.tinggal} ${fileData.perkawinan}`);
    });
}

const detailContact = (nama) => {
    const fileData      = loadContact();    
    const contact       = fileData.find((contact) => contact.nama.toLowerCase() === nama.toLowerCase());

    if(!contact){
        console.log(`${nama} tidak ada di List`);
        return false;
    }

    console.log(contact.nama);
    console.log(contact.tinggal);
    console.log(contact.perkawinan);

}

const deleteContact = (nama) => {
    const fileData      = loadContact();   
    const newContacts   = fileData.filter(
        (contact) => contact.nama.toLowerCase() !== nama.toLowerCase()
        ); 

    if(fileData.length === newContacts.length){
        console.log(`${nama} Tidak Di temukan`);
    }

    fs.writeFileSync('data/contacts.json', JSON.stringify(newContacts, null, 2), (err) => {
        console.log(err);
    });

    console.log(`data ${nama} berhasil di hapus.`);

}

module.exports ={listContact, simpanContact, detailContact, deleteContact }