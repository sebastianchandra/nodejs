const fs = require('fs');

// menuliskan string ke file secara synchronus
// try{
// 	fs.writeFileSync('data/test.txt','Hello Bro secara Synchronus')
// } catch(e){
// 	console.log(e);
// }

// menuliskan string file secara asynchronus
// fs.writeFile('data/test.txt', 'hellow word brooooood',(err) => {
// 	console.log(err);
// })

//membaca isi file secara syncronus
// const data = fs.readFileSync('data/test.txt', 'utf-8');
// console.log(data);

//membaca isi file secara asyncronus
// fs.readFile('data/test.txt', 'utf-8',(err, data) => {
//   if (err) throw err;
//   console.log(data);
// });


//readline
const readline = require('readline');

const rl = readline.createInterface({
  	input: process.stdin,
  	output: process.stdout
});

// Membuat Folder Data.
const dirPath = './data';
if(!fs.existsSync(dirPath)){
	fs.mkdirSync(dirPath);
}

// Membuat File contacts.json jika belom ada
const dataPath = './data/contacts.json';
if(!fs.existsSync()){
	fs.writeFileSync(dataPath, '[]', 'utf-8');
}

rl.question('Nama Lu sape ? ', (answer1) => {
  	// TODO: Log the answer in a database
  	console.log(`Ooh ${answer1}`);

  	// rl.close();
 	rl.question(`Tinggal Dimane ${answer1} ? `, (answer2) => {
		console.log(`ooh Tinggal di ${answer2}`);
		console.log(`Makasih ye ${answer1} udah jauh dari ${answer2}`);

		const nama 		= answer1;
	 	const tinggal 	= answer2;

	 	// const jawaban 	= `Nama ${answer1} Tinggal di ${answer2}`;
	 	// const jawaban 	= `"employees": {"nama":"${answer1}", "lastName":"${answer2}"}`;
	 	
	 	const jawaban 	= {
	 		nama 	: nama,
	 		tinggal : tinggal
	 	}

	 	const fileJson 	= fs.readFileSync('data/contacts.json', 'utf8');
	 	const fileSave  = JSON.parse(fileJson);

	 	fileSave.push(jawaban);

	 	fs.writeFileSync('data/contacts.json', JSON.stringify(fileSave, null, 2), (err) => {
	 		console.log(err);
	 	});

	 	console.log(fileSave);
		rl.close();
	});

});




