const contacts = require('./contacts');

const main = async () => {

    const nama = await contacts.tanya('Nama Lu sape ? ');
    const tinggal = await contacts.tanya(`${nama}, Lu Tinggal Dimane ? `);
    let perkawinan = await contacts.tanya(`${nama}, Lu Udah Nikah ? `);
    if(perkawinan=='udah'){
        var anak = await contacts.tanya(`${nama}, Anak Lu Udah berapa ? `);
        var tgl_nikah = '';
    }else{
        var tgl_nikah = await contacts.tanya(`${nama}, Kapan Nikah ? `);
        var anak = '';
    }

    contacts.simpanContact(nama, tinggal, perkawinan, anak, tgl_nikah);

}

main();
