const { string } = require("yargs");
const yargs = require("yargs");
const contacts = require('./contacts');
// console.log(yargs.argv);

yargs.command({
    command: 'add',
    describe : 'Menambahkan Kontak Baru',
    builder: {
        nama: {
            describe: "Nama Lengkap",
            demandOption: true,
            type: 'string',
        },
        tinggal: {
            describe: "Tempat Tinggal",
            demandOption: true,
            type: 'string',
        },
        perkawinan: {
            describe: "Status Perkawinan",
            demandOption: true,
            type: 'string',
        },
        anak: {
            describe: 'Jumlah Anak',
            demandOption: false,
            type: 'string',
        },
        tgl_nikah: {
            describe: 'Tanggal Pernikahan',
            demandOption: false,
            type: 'string',
        },
    },
    handler(argv){
        const contact = {
            nama: argv.nama,
            tinggal: argv.tinggal,
            perkawinan: argv.perkawinan,
            anak: argv.anak,
            tgl_nikah: argv.tgl_nikah,
        }
        
        contacts.simpanContact(contact.nama, contact.tinggal, contact.perkawinan, contact.anak, contact.tgl_nikah);
    }
}).demandCommand();

// Menampilkan daftar semua nama contact
yargs.command({
    command: 'list',
    describe : 'Menampilkan Daftar Contact',
    handler() {
        contacts.listContact();
    }
});

// Menampilkan detail contact
yargs.command({
    command: 'detail',
    describe : 'Menampilkan detail Contact Berdasarkan Nama',
    builder: {
        nama: {
            describe: "Nama Lengkap",
            demandOption: true,
            type: 'string',
        },
    },
    handler(e) {
        contacts.detailContact(e.nama);
    }
});

// Kontak detail berdasarkan Nama
yargs.command({
    command: 'delete',
    describe : 'Menghapus Contact Berdasarkan Nama',
    builder: {
        nama: {
            describe: "Nama Lengkap",
            demandOption: true,
            type: 'string',
        },
    },
    handler(e) {
        contacts.deleteContact(e.nama);
    }
});

yargs.parse();