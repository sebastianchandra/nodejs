const express = require('express')
const app = express()
const port = 3000

app.get('/', (req, res) => { 
//   res.send('Hello World!')
    // res.json({
    //     nama: 'Sebastian',
    //     email: 'Sebastianchandra@gmail.com',
    //     noHp: '000000000'
    // });
    res.sendFile('./index.html', {root: __dirname});
});
app.get('/about', (req, res) => {
//   res.send('Hello World About!')
    res.sendFile('./about.html', {root: __dirname});
});

app.get('/contact', (req, res) => {
//   res.send('Hello World contact!')
    res.sendFile('./contact.html', {root: __dirname});
})

// app.get('/product/:id/category/:id_cat', (req, res) => {
//     res.send(`Product ID : ${req.params.id} </br> Kategory ID: ${req.params.id_cat}`);
// });
app.get('/product/:id', (req, res) => {
    res.send(`Product ID : ${req.params.id} </br> Kategory ID: ${req.query.category}`);
});

app.use('/',(req,res)=>{
    res.status('404');
    res.send('test');
});

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})



























// const fs = require('fs');
// const http = require('http');
// const port = 3000;

// const renderHTML = (path, res) => {
//     fs.readFile(path, (e,d) => {
//         console.log(e);
//         if(e){
//             res.writeHead(404);
//             res.write('Halaman Ga ada Cuk');
//         }else{
//             res.write(d);
//         }
//         res.end();
//     });
// }

// const server = http.createServer((req, res) => {    
//     res.writeHead(200, {
//         'Content-Type' : 'text/html',
//     })

//     const url = req.url;

//     switch(url) {
//         case '/about':
//             renderHTML('./about.html',res);
//             break;
//         case '/contact':
//             renderHTML('./contact.html',res);
//             break;
//         default:
//             renderHTML('./index.html', res);
//             break;
//     }



//     // if(url === '/about'){
//     //     renderHTML('./about.html', res)
//     // }else if(url === '/contact'){
//     //     renderHTML('./contact.html', res);
//     // }else {
//     //     renderHTML('./index.html',res);
//     // }

    
// });

// server.listen(port, () => {
//     console.log(`server is listening on port ${port}.`);
// });